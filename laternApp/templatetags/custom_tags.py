from django import template
from laternApp.models import *

register = template.Library()

@register.filter
def get_comments(post_id):
    comments = Posts.objects.get(id=post_id).comments.all().order_by('-id')
    if comments:
        return comments
    else:
        return False


@register.filter
def get_votes(post_id):
    post = Posts.objects.get(id=post_id)
    count = post.upvotes.count() - post.downvotes.count()
    if count:
        return count
    else:
        return 0

@register.filter
def get_mood_name(mood_ids):
    mood_ids = mood_ids.split(',')

    try:
        moods = MoodTags.objects.filter(id__in=list(mood_ids)).values_list('mood', flat=True)
        return ', '.join(moods)
    except:
        return 'No Tags'


@register.filter
def upvote(post, user_id):
    if (user_id in post.upvotes.values_list('id',flat=True)):
        return 'black'
    else:
        return 'dark-grey'

@register.filter
def downvote(post, user_id):
    if (user_id in post.downvotes.values_list('id',flat=True)):
        return 'black'
    else:
        return 'dark-grey'