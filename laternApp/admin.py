from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Users)
admin.site.register(Posts)
admin.site.register(Comments)
admin.site.register(Roles)
admin.site.register(Genre)
admin.site.register(MoodTags)
admin.site.register(PostMood)