from django import forms
from .models import *

class UserForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'required': True, 'maxlength': 30}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'required': True, 'maxlength': 30}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'required': True, 'maxlength': 30}))
    email = forms.CharField(label='email', max_length=256, widget=forms.HiddenInput())
    profile_pic = models.ImageField()

    class Meta:
        model = Users
        fields = ('username', 'password', 'email','profile_pic')


class PostForm(forms.ModelForm):
    body_text = forms.CharField(max_length=80, required=False,widget=forms.TextInput(attrs={'class': 'form-control', 'required': True}))
    description = forms.CharField(max_length=80, required=False,
                                widget=forms.TextInput(attrs={'class': 'form-control'}))
    file = forms.FileField(required=False)
    genre = forms.ChoiceField(choices=Genre.GENRE_CHOICES, required=True, label='Photography', widget=forms.Select(attrs={'class':'mdb-select'}))
    story = forms.CharField(widget=forms.Textarea(attrs={'class': 'md-textarea', 'required': False, 'style': "min-height: 20rem;overfow-y: scroll;resize: none;"}), required=False)

    class Meta:
        model = Posts
        fields = ('file', 'body_text','heading','description','genre')


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password =  forms.CharField(widget=forms.PasswordInput)


class ProfileForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'required': True}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control', 'required': True}))
    about_me = forms.CharField(widget=forms.Textarea(attrs={'class': 'md-textarea', 'required': True}))
    website_link = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'aria-describedby': 'basic-addon3'}), required=False)
    facebook_link = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'aria-describedby': 'basic-addon3'}), required=False)
    twitter_link = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'aria-describedby': 'basic-addon3'}), required=False)
    google_plus_link = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'aria-describedby': 'basic-addon3'}), required=False)
    linkedin_link = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'aria-describedby': 'basic-addon3'}), required=False)
    dribble_link = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'aria-describedby': 'basic-addon3'}), required=False)


