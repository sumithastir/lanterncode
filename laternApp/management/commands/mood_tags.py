from django.core.management.base import BaseCommand, CommandError
from laternApp.models import *
import csv
# import additional classes/modules as needed


class Command(BaseCommand):
    help = 'Update the Mood Tag Model'

    def csv_reader(self, file_obj):
        """

        Read a csv file

        """
        mood_types = {
            '0': 'Neutral',
            '1': 'Positive',
            '-1': 'Negative'
        }
        mood_tags = []
        reader = csv.reader(file_obj)
        for index, mood_tag in enumerate(reader):
            if index == 0:
                continue
            mood_tags.append(MoodTags(mood=mood_tag[0], emotion=mood_tag[1],mood_type=mood_types[mood_tag[2]]))

        MoodTags.objects.bulk_create(mood_tags)


    def handle(self, *args, **options):

        print('Start '+ self.help)
        MoodTags.objects.all().delete()

        csv_path = "laternApp/static/assets/Lantern LexiconSheet1.csv"

        with open(csv_path, "rt") as f_obj:
            self.csv_reader(f_obj)

        print('Updated')