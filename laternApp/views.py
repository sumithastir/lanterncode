import os , shutil
import random
import json
import string
import random
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponse , HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from Lantern.settings import BASE_DIR
from laternApp.models import *
from .forms import UserForm , PostForm , LoginForm, ProfileForm
from django.contrib.auth import logout
from django.forms.models import model_to_dict
from django.http import Http404
from Lantern import settings
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from django.contrib import messages
from django.db.models import Count

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def send_email(subject, to=None, password=None):
    from_email = settings.EMAIL_FROM_EMAIL
    SITE_LINK = settings.SITE_LINK
    context = {
        'SITE_LINK': SITE_LINK,
        'password': password
    }

    message = get_template('laternApp/email_templates/forgot_password_email.html').render(Context(context))
    html_content = message
    msg = EmailMultiAlternatives(subject, subject, from_email, to)
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def landing(request):
    user = request.user
    if user.is_active and user.is_authenticated:
        return render(request,'laternApp/home.html', {'header_emotion':'no'})
    else :
        return render(request, 'laternApp/land.html', {})

# Class view for SignUp Page and redirecting to the Email Validation page
class UserFormView(View):
    form_class = UserForm
    template = 'laternApp/register.html'

    def get(self,request):
        email = request.GET['email']
        form = self.form_class(initial={'email': email})

        return render(request,self.template,{'form':form})

    def post(self,request):
        form = UserForm(request.POST, request.FILES)

        if form.is_valid():

            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            profile_pic = request.FILES.get('profile_pic','')
            users = Users(username=username, email=email, profile_pic=profile_pic)
            users.set_password(password)
            users.save()
            user = authenticate(username=username, password=password)

            if user \
                    is not None:
                if user.is_active:
                    login(request, user)
                    request.user.users = users
                    return HttpResponseRedirect('/profile/?model=edit_profile')
        else:
            return HttpResponseRedirect('/')

# Sending a validation mail to the user
#Schema followed :- 1. A token is generated in confirmed section and that is stored in the confirm column
#                   2. When user visits the Url then that email is confirmed.
def validateEmail(request):
    pass

# Function to get signin and then redirect to the Home Page
def loginView(request):
    form = LoginForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                users = Users.objects.get(id=request.user.id)
                request.user.users = users
                return HttpResponseRedirect('/')
        else:
            loginerror = "Username or password incorrect"
            return render(request, 'laternApp/land.html', {'loginerror':loginerror})
    return HttpResponse("Somethning went wrong.Please try after sometime.")


# Function for signup and then redirect to the confirn mail page
def signupView(request):
    if 'email' in request.POST:
        return HttpResponseRedirect('/signup/?email='+request.POST.get('email',''))
    else:
        return HttpResponse("Register Failed")


def signup(request):
    form = UserForm(request.POST)

    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        email = form.cleaned_data['email']

        user =  Users(username=username, email=email)
        user.set_password(password)

        user.save()

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                user = Users.objects.get(id=request.user.id)
                userid = user.id
                os.makedirs(os.path.join(BASE_DIR, 'laternApp/static/profileData/' + str(userid) + '/postdata'))

                if request.FILES:
                    profile_pic = request.FILES['profile_pic']
                    user.profile_pic = profile_pic
                    file = Users.objects.filter(id=userid)[0].profile_pic
                    filename = str(file)
                    filename = filename.split('/', 2)[1]
                    src = os.path.join(BASE_DIR, filename)
                    temp = 'laternApp/static/profileData/' + str(userid) + '/profile'
                    dest = os.path.join(BASE_DIR, temp)
                    shutil.move(src, dest)

                else:

                    random_num = random.randrange(1,7)
                    src = os.path.join(BASE_DIR, 'laternApp/static/assets/img/avatar'+ str(random_num)+ '.jpg')
                    temp = 'laternApp/static/profileData/' + str(userid) + '/profile'
                    dest = os.path.join(BASE_DIR, temp)
                    shutil.copy2(src, dest)


                return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def logoutView(request):
    logout(request)
    return HttpResponseRedirect('/')

class ProfileView(View):
    form_class = ProfileForm
    template = 'laternApp/profile.html'


    def get(self,request):
        context = {}
        if 'model' in request.GET:
            context['model'] = request.GET['model']
        context['form'] = self.form_class(initial=model_to_dict(request.user.users))
        posts = Posts.objects.filter(user=request.user).order_by('-id')
        context['posts'] = posts
        return render(request,self.template,context)

    def post(self,request):

        form = self.form_class(request.POST)
        if form.is_valid():
            Users.objects.filter(id=request.user.id).update(**form.cleaned_data)
            users = Users.objects.get(id=request.user.id)
            request.user.users = users
            return HttpResponse(json.dumps({'response': 'Profile Updated successfully'}), content_type="application/json")
        else:
            return HttpResponseRedirect('/')


class PublicProfileView(View):
    template = 'laternApp/public_profile.html'

    def get(self, request, username):
        try:
            users = Users.objects.get(username=username)
            posts = Posts.objects.filter(user_id=users.id, allow=1).order_by('-id')

            return render(request, self.template, {'users': users, 'posts': posts})
        except:
            raise Http404("Invalid Username")

class ProfilePictureView(View):
    template = 'laternApp/profile_picture.html'

    def get(self, request):
        try:
            return render(request, self.template, {})
        except:
            raise Http404("Invalid Username")

    def post(self, request):
        if 'profile_pic' in request.FILES:
            profile_pic = request.FILES.get('profile_pic', '')
            users = request.user.users
            users.profile_pic = profile_pic
            users.save()
            return HttpResponseRedirect('/profile/')


def forgot_password_view(request):
    if 'email' in request.POST:
        try:
            users = Users.objects.get(email=request.POST['email'])
        except:
            raise Http404("Email does not exists!!")
        password = id_generator()
        users.set_password(password)
        users.save()
        send_email('Forgot Password Email', [users.email], password=password)
        return HttpResponseRedirect('/')

def change_password_view(request):
    if 'password' in request.POST:
        try:
            users = Users.objects.get(id=request.user.id)
        except:
            raise Http404("Email does not exists!!")
        password = request.POST['password']

        users.set_password(password)
        users.save()
        user = authenticate(username=request.user.username, password=password)  # <-- here!!
        if user is not None:
            login(request, user)
        return HttpResponseRedirect('/')


# test function for dashboard to view the entire posts in the database and make submission simultaneously
def dashboard(request, emotion):
    form = PostForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        username = request.user.username

        postfrm = Users.objects.filter(username=username)[0]
        text =form.cleaned_data['body_text']
        allow = False
        image = None
        if request.FILES :
            image = request.FILES['image']
        tweet = Posts( image=image ,body_text=text,allow=allow,user=postfrm)
        tweet.save()

        form = PostForm(None)
        posts = Posts.objects.all()
        return render(request , 'laternApp/index.html' , {'posts' : posts ,  'form': form})
    else:
        ## If emotion is angry or sad
        ## Then create a custom list of pieces
        ## else get pieces list filter by emotion and allow = 1
        if emotion == 'angry' or emotion == 'sad':
            ## Get the Post mood list of all three mood type filtr by emotion
            positive_moods = PostMood.objects.filter(post__emotion__icontains=emotion, post__allow=1,
                                                 mood__mood_type='Positive').values('post').order_by('-post__vote_counter',
                                                                                      '-post__id').distinct()
            negative_moods = PostMood.objects.filter(post__emotion__icontains=emotion, post__allow=1,
                                                     mood__mood_type='Negative').values('post').order_by(
                '-post__vote_counter', '-post__id').distinct()

            neutral_moods = PostMood.objects.filter(post__emotion__icontains=emotion, post__allow=1,
                                                     mood__mood_type='Neutral').values('post').order_by(
                '-post__vote_counter', '-post__id').distinct()

            post_moods = []
            negative_index = 0
            neutral_index = 0
            ## Merge the Postitve Negative aane Neutral Pieces
            for index,posi_mood in enumerate(positive_moods):
                index= index+1
                post_moods.append(posi_mood['post'])
                if index and index % 5 == 0:
                    if len(neutral_moods) > neutral_index:

                        post_moods.append(neutral_moods[neutral_index]['post'])
                        neutral_index = neutral_index + 1

                    if len(negative_moods) > negative_index:
                        for negi_mood in negative_moods[negative_index:]:
                            post_moods.append(negi_mood['post'])
                            negative_index = negative_index + 1
                            if negative_index % 5 == 0:
                                break
            posts = Posts.objects.filter(id__in=post_moods)
            objects = dict([(obj.id, obj) for obj in posts])
            ## Sort the piece lit according to piece id list get after merge of mood type
            sorted_objects = [objects[id] for id in post_moods]

            posts = sorted_objects
        else:
            posts = Posts.objects.filter(emotion__icontains=emotion, allow=1).order_by('-vote_counter', '-id')
        return render(request, 'laternApp/index.html', {'posts': posts, 'form': form})


# Welcome page after login
def welcome(request):
    return render(request,"index.html",{"dummy":"hello"})


# Function to be called for curation view
def curateView(request,message=""):
    mood_list = MoodTags.objects.all().values('id', 'mood')
    curator_post = CuratorPost.objects.filter(post__allow=0,post__genre=request.user.users.genre_name,curator=request.user).order_by('-id')
    return render(request,"laternApp/curate.html",{"curator_post":curator_post,'message':message, 'mood_list': mood_list})


# Function to Approve the curate
def curateAllow(request,postid):
    p = Posts.objects.filter(id=postid)[0]
    comment = request.POST['comment']
    tags = request.POST.getlist('tags')
    rate = request.POST['rate']

    emotion = moodCalculator(tags=tags, post=p)
    p.allow =1
    p.curator_comments = comment
    p.mood = ','.join(tags)
    p.emotion = emotion
    p.rate = rate
    p.save()
    message = "Post " + str(p.heading) + " allowed."
    return curateView(request, message)

# Function to curate (Delete) the post
def curateDelete(request,postid):
    p = Posts.objects.filter(id=postid)[0]
    p.allow = 2
    p.save()
    message = "Post " + str(p.heading) + " Rejected"
    return curateView(request,message)

# Function to Get the emotion list based on mood tags
def moodCalculator(tags, post):
    mood_list = MoodTags.objects.filter(id__in=list(tags))
    post_mood = []
    emotion_tags = []
    for mood in mood_list:
        post_mood.append(PostMood(post=post, mood=mood))
        emotion = mood.emotion
        multiple_emotion = emotion.split(',')
        for emotion_tag in multiple_emotion:
            emotion_tags.append(emotion_tag)
    PostMood.objects.bulk_create(post_mood)
    distinct_list = list(set(emotion_tags))
    return " ".join(distinct_list)


## Function to Submit the Piece
def submit(request):
    form = PostForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        message = "Your piece has been sent to one of our curators!"
        body_text = form.cleaned_data['body_text']
        heading = form.cleaned_data['heading']
        description = form.cleaned_data['description']
        story = form.cleaned_data['story']
        file = ''
        if 'file' in request.FILES and request.FILES['file']:
            file = request.FILES['file']
        post = Posts(
            heading=heading,
            body_text=body_text,
            description=description,
            file=file,
            story=story,
            user=request.user.users,
            genre=form.cleaned_data['genre'],
            my_work=request.POST['my_work'],
            artist = request.POST['artist']
        )
        post.save()
        if post:
            usersid = Users.objects.filter(genre_name=form.cleaned_data['genre']).values_list('id', flat=True)
            curators = User.objects.extra(
                select={'val': "SELECT Count(*) FROM laternApp_curatorpost WHERE curator_id=auth_user.id"},
                select_params=('id',), ).filter(id__in=usersid).values('id', 'val').order_by('val')
            curator_post = CuratorPost()
            curator_post.post_id = post.id
            curator_post.curator_id = curators[0]['id']
            curator_post.save()

        messages.add_message(request, messages.SUCCESS, message)
        return HttpResponseRedirect('/')
    else:
        return render(request, 'laternApp/submit.html', {'form': form})


def homeView(request,mood=0):
    posts = Posts.objects.filter(allow=1).filter(mood=mood)
    return render(request, "laternApp/index.html", {"posts": posts})


## Function to add comment on the piece
@csrf_exempt
def commentAdd(request,postid):
    post = Posts.objects.get(id=postid)
    user = Users.objects.get(id=request.user.id)
    body = request.POST['text']
    comment = Comments(author_id= user , body= body)
    comment.save()
    post.comments.add(comment)
    post.save()
    userid = user.id
    username = user.username
    # if request.user.users.profile_pic:
    #     profile_pic = request.user.users.profile_pic
    # else:
    #     profile_pic = 'profileData/avatar.jpeg';
    # dict = username + ':::' + str(userid).strip() + ':::' + body  + ':::' + profile_pic
    dict = username + ':::' + str(userid).strip() + ':::' + body
    return HttpResponse(dict)


## Function to give upvote to piece
@csrf_exempt
def upVote(request, postid):
    post = Posts.objects.get(id=postid)
    user = Users.objects.get(id=request.user.id)

    if (user.id in post.upvotes.values_list('id',flat=True)):
        post.upvotes.remove(user)
    else:
        if user.id in post.downvotes.values_list('id', flat=True):
            post.downvotes.remove(user)
        post.upvotes.add(user)

    post.save()
    count = post.upvotes.count() - post.downvotes.count()
    post.vote_counter = count
    post.save()
    return HttpResponse(count)


## Function to give downvote to piece
@csrf_exempt
def downVote(request, postid):
    post = Posts.objects.get(id=postid)
    user = Users.objects.get(id=request.user.id)

    if user.id in post.downvotes.values_list('id',flat=True):
        post.downvotes.remove(user)
    else:
        if (user.id in post.upvotes.values_list('id', flat=True)):
            post.upvotes.remove(user)
        post.downvotes.add(user)
    count = post.upvotes.count() - post.downvotes.count()
    post.vote_counter = count
    post.save()
    return HttpResponse(count)

# Welcome page after login
def sorry(request):
    return render(request,"laternApp/sorry.html",{})