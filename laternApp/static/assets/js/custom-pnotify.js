function show_stack_bar_bottom(type, text) {
  var opts = {
      title: "Over Here",
      text: "Check me out. I'm in a different stack.",
      addclass: "stack-bar-top",
      cornerclass: "",
      width: "80%",
      stack: {"dir1":"down", "dir2":"right", "push":"top"},
      delay: 2000
  };
  switch (type) {
  case 'error':
      opts.title = type;
      opts.text = text;
      opts.type = "error";
      break;
  case 'info':
      opts.title = type;
      opts.text = text;
      opts.type = "info";
      break;
  case 'success':
      opts.title = type;
      opts.text = text;
      opts.type = "success";
      break;
  }
  new PNotify(opts);
}