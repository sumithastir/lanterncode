# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-26 14:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('laternApp', '0022_auto_20170426_0358'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostMood',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mood', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='post_mood', to='laternApp.MoodTags')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laternApp.Posts')),
            ],
        ),
    ]
