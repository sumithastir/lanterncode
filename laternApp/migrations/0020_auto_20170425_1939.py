# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-25 19:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('laternApp', '0019_posts_vote_counter'),
    ]

    operations = [
        migrations.CreateModel(
            name='RatingPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rate', models.IntegerField(null=True)),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laternApp.Posts')),
                ('rate_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rate_by', to='laternApp.Users')),
            ],
        ),
        migrations.AddField(
            model_name='moodtags',
            name='mood_type',
            field=models.CharField(choices=[(1, b'Positive'), (-1, b'Negative'), (0, b'Neutral')], default=1, max_length=20),
        ),
    ]
