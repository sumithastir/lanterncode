# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laternApp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comments',
            name='post_id',
        ),
        migrations.AddField(
            model_name='posts',
            name='comments',
            field=models.ManyToManyField(default=None, to='laternApp.Comments', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='comments',
            name='image',
            field=models.ImageField(null=True, upload_to=b'', blank=True),
        ),
    ]
