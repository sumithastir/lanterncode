# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-12 14:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laternApp', '0008_posts_story'),
    ]

    operations = [
        migrations.AlterField(
            model_name='genre',
            name='genre_name',
            field=models.CharField(choices=[(b'Photography', b'Photography'), (b'Art', b'Art'), (b'WrittenWord', b'Written Word')], max_length=20),
        ),
        migrations.AlterField(
            model_name='posts',
            name='allow',
            field=models.IntegerField(default=0),
        ),
    ]
