# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('body', models.TextField(max_length=1000)),
                ('image', models.ImageField(null=True, upload_to=b'')),
            ],
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('genre_name', models.CharField(max_length=20, choices=[(b'Photography', b'Photography'), (b'Art', b'Art')])),
            ],
        ),
        migrations.CreateModel(
            name='Posts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('updates_at', models.DateTimeField(auto_now=True)),
                ('mood', models.IntegerField(default=0)),
                ('curator_comments', models.CharField(max_length=300, blank=True)),
                ('allow', models.BooleanField(default=False)),
                ('isImage', models.BooleanField(default=0)),
                ('heading', models.CharField(max_length=80, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('description', models.CharField(max_length=400)),
                ('body_text', models.TextField(default=None)),
                ('file', models.FileField(null=True, upload_to=b'')),
                ('genre', models.CharField(max_length=20, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Roles',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role_id', models.IntegerField(default=1)),
                ('permissions', models.IntegerField(default=111)),
                ('name', models.CharField(max_length=50)),
                ('users', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('role', models.IntegerField(default=1)),
                ('confirmed', models.BooleanField(default=False)),
                ('profile_pic', models.ImageField(null=True, upload_to=b'')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AddField(
            model_name='posts',
            name='downvotes',
            field=models.ManyToManyField(default=None, related_name='downvotes', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='posts',
            name='upvotes',
            field=models.ManyToManyField(default=None, related_name='upvotes', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='posts',
            name='user',
            field=models.ForeignKey(related_name='userid', to='laternApp.Users'),
        ),
        migrations.AddField(
            model_name='genre',
            name='curators',
            field=models.ManyToManyField(to='laternApp.Users', blank=True),
        ),
        migrations.AddField(
            model_name='comments',
            name='author_id',
            field=models.ForeignKey(to='laternApp.Users'),
        ),
        migrations.AddField(
            model_name='comments',
            name='post_id',
            field=models.ForeignKey(to='laternApp.Posts'),
        ),
    ]
