# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-28 13:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laternApp', '0025_auto_20170427_1933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='posts',
            name='file',
            field=models.ImageField(null=True, upload_to=b'postData/'),
        ),
    ]
