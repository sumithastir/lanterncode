from django.contrib.auth.models import User
from django.http import HttpResponse


def is_user_exists(request):
    if 'username' in request.GET:
        username = request.GET['username']
        if User.objects.filter(username__iexact=username):
            return HttpResponse("false")
        else:
            return HttpResponse('true')


def is_email_exists(request):
    if 'email' in request.GET:
        email = request.GET['email']
        if User.objects.filter(email__iexact=email):
            return HttpResponse('false')
        else:
            return HttpResponse('true')

def forgot_email_exists(request):
    if 'email' in request.GET:
        email = request.GET['email']
        if User.objects.filter(email__iexact=email):
            return HttpResponse('true')
        else:
            return HttpResponse('false')

def check_password(request):
    if 'old_password' in request.GET:
        old_password = request.GET['old_password']
        if not request.user.check_password(old_password):
            return HttpResponse("false")
        else:
            return HttpResponse('true')