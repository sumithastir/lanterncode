from django.apps import AppConfig


class LaternappConfig(AppConfig):
    name = 'laternApp'
