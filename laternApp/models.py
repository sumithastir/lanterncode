from django.db import models
from django.contrib.auth.models import User
from PIL import Image as Img

from io import StringIO, BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile

# Create your models here.


#Role_id is 1 for simpleUsers, 2 for Curators and 3 for admin.
class Roles(models.Model):
    role_id = models.IntegerField(default=1) # same as that for the Users
    permissions = models.IntegerField(default=111)  # string of 0 and 1 to determine the permissions
    name = models.CharField(max_length=50)
    users = models.IntegerField(default=0)


#Class for Users.
class Users(User):
    GENRE_CHOICES = (
        ('Photography', 'Photography'),
        ('Art', 'Art'),
        ('WrittenWord', 'Written Word'),
    )
    # Since its a user object it already cointains the Username, Password_hash, Firstname, Lastname, Email and an Id(PrimaryKey)
    role = models.IntegerField(default=1)

    confirmed = models.BooleanField(default=False)
    profile_pic = models.ImageField(upload_to = 'profileData/', default='profileData/avatar.jpeg', blank=True, null=True)
    about_me = models.TextField(null=True, blank=True)
    website_link = models.TextField(null=True, blank=True)
    facebook_link = models.TextField(null=True, blank=True)
    twitter_link = models.TextField(null=True, blank=True)
    google_plus_link = models.TextField(null=True, blank=True)
    linkedin_link = models.TextField(null=True, blank=True)
    dribble_link = models.TextField(null=True, blank=True)
    genre_name = models.CharField(max_length=20, choices=GENRE_CHOICES, null=True, blank=True)

#Genre Class Needed to decide whom to send the post for curation
class Genre(models.Model):
    GENRE_CHOICES = (
        ('Photography', 'Photography'),
        ('Art', 'Art'),
        ('WrittenWord', 'Written Word'),
    )
    genre_name = models.CharField(max_length=20,choices=GENRE_CHOICES)
    curators = models.ManyToManyField(Users,blank=True)


#Class for comments
class Comments(models.Model):
    created_at =  models.DateTimeField(auto_now_add=True)
    author_id = models.ForeignKey(Users,on_delete=models.CASCADE)
    body = models.TextField(max_length=1000)
    image = models.ImageField(null=True,blank=True)


#Class for posts
class Posts(models.Model):

    updates_at = models.DateTimeField(auto_now=True)
    mood = models.TextField(default='', null=True)
    emotion = models.TextField(default='', null=True)
    rate = models.IntegerField(null=True,default=0)
    #Unique check has to be applied
    upvotes =  models.ManyToManyField(User,related_name='upvotes', default=None)
    downvotes =  models.ManyToManyField(User,related_name='downvotes', default=None)
    vote_counter = models.FloatField(default=0)

    curator_comments = models.CharField(max_length=300,blank=True)
    allow = models.IntegerField(default=0)
    my_work = models.BooleanField(default=False)
    isImage = models.BooleanField(default=0)
    artist = models.CharField(max_length=255, blank=True, null=True)
    story = models.TextField(null=True)

    user = models.ForeignKey(Users,related_name='userid', on_delete=models.CASCADE)
    heading = models.CharField(max_length=80, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=400)
    body_text =  models.TextField(default=None)
    file = models.ImageField(upload_to = 'postData/', null=True)
    genre = models.CharField(max_length=20, blank=True)
    comments =  models.ManyToManyField(Comments,blank=True,default=None,null=True, related_name='comments')

    def save(self, *args, **kwargs):
        if self.file:
            img = Img.open(BytesIO(self.file.read()))
            if img.mode != 'RGB':
                img = img.convert('RGB')
            img.thumbnail((self.file.width / 1.5, self.file.height / 1.5), Img.ANTIALIAS)
            output = BytesIO()
            img.save(output, format='JPEG', quality=70)
            output.seek(0)
            self.file = InMemoryUploadedFile(output, 'ImageField', "%s.jpg" % self.file.name.split('.')[0],
                                              'image/jpeg', output.tell(), None)
        super(Posts, self).save(*args, **kwargs)

class CuratorPost(models.Model):
    post = models.ForeignKey(Posts)
    curator = models.ForeignKey(Users,related_name='curator', on_delete=models.CASCADE)


class MoodTags(models.Model):
    MOOD_TYPE = (
        ('Positive', 'Positive'),
        ('Negative', 'Negative'),
        ('Neutral', 'Neutral'),
    )
    mood_type = models.CharField(max_length=20,default=MOOD_TYPE[0][0] , choices=MOOD_TYPE)
    mood = models.CharField(max_length=255, blank=True)
    emotion = models.CharField(max_length=255, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.mood

class PostMood(models.Model):
    post = models.ForeignKey(Posts)
    mood = models.ForeignKey(MoodTags,related_name='post_mood', on_delete=models.CASCADE)
