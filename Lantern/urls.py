"""Lantern URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from laternApp.views import *
from laternApp.core.custom_validation import *
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', landing, name='landing'),
    url(r'^dash/(?P<emotion>[a-z]+)$',dashboard, name='dashboard'),

    url(r'^login/', loginView),
    url(r'^logout/', logoutView, name='logout'),
    url(r'^register/',signupView),
    url(r'^signup/', UserFormView.as_view()),
    url(r'^public_profle/(?P<username>[0-9a-z]+)', PublicProfileView.as_view(), name='public_profile'),
    url(r'^profile/', login_required(ProfileView.as_view(), login_url='/'), name='profile'),
    url(r'^profile_picture/', ProfilePictureView.as_view(), name='profile_picture'),
    url(r'^forgot_password_view/', forgot_password_view, name='forgot_password_view'),
    url(r'^change_password_view/', change_password_view, name='change_password_view'),

    url(r'^curate/$', curateView, name='curate'),
    url(r'^curate/curateDelete/(?P<postid>[0-9]+)', curateDelete),
    url(r'^curate/curateAllow/(?P<postid>[0-9]+)', curateAllow),

    url(r'^submit/', submit, name='submit'),


    url(r'^comments/add/(?P<postid>[0-9]+)', commentAdd),
    url(r'^upvote/(?P<postid>[0-9]+)', upVote),
    url(r'^downvote/(?P<postid>[0-9]+)', downVote),

    url(r'^home/$',homeView),
    url(r'^home/(?P<mood>[1-6])',homeView),
    url(r'^sorry/', sorry, name='sorry'),
    #validation check
    url(r'^is_user_exists/', is_user_exists, name='is_user_exists'),
    url(r'^check_password/', check_password, name='check_password'),
    url(r'^is_email_exists/', is_email_exists, name='is_email_exists'),
    url(r'^forgot_email_exists/', forgot_email_exists, name='forgot_email_exists'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
