# README #

1) Run python3 manage.py makemigrations, python3 manage.py migrate and python3 manage.py mood_tags.

2) Create an admin user using python manage.py createsuperuser.

3) Run the server using python manage.py runserver

4) Go the 127.0.0.1:8000 and create three differnet accounts (keep logging out and creating accounts). 

5) Logout and goto 127.0.0.1:8000/admin.

6) Login with the admin user details.

7) Click on users under lanternapp and and choose the first of the three accounts you created and scroll down to the bottom. 

8) For genre name, select one of the three from the drop down menu. 

9) Follow steps 7 and 8 for the two other  accounts so that they can be the curators for the two remaining genres. 

10) Once you have three functioning curator accounts, you will be able to use all the functionality freely. 